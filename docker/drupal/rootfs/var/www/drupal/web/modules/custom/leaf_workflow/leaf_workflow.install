<?php

/**
 * @file
 * Install, update and uninstall functions for the content_sync module.
 */

use Drupal\Core\Database\Database;

/**
 * Implements hook_schema().
 */
function leaf_workflow_schema() {
  $schema['leaf_workflow'] = [
    'description' => 'Leaf workflow schema.',
    'fields' => [
      'leaf_workflow_id' => [
        'description' => 'Primary identifier for User',
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ],
      'node_id' => [
        'description' => 'Node ID',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ],
      'revision_id' => [
        'description' => 'Revision ID.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ],
      'public_visibility' => [
        'description' => 'Public visibility',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ],
      'moderation_state' => [
        'description' => 'Moderation State',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ],
      'activity' => [
        'description' => 'Activity',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ],
      'is_activity_completed' => [
        'description' => 'Is activity completed',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ],
      'revision_note' => [
        'description' => 'Revision Note',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ],
      'assignee' => [
        'description' => 'Assign to',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ],
      'created_date' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'mysql_type' => 'datetime',
        'description' => 'Created date.',
      ],
      'author_by' => [
        'type' => 'varchar',
        'description' => "Author By",
        'length' => 255,
        'not null' => FALSE,
      ],
      'media_json_value' => [
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'description' => 'Media json string.',
      ],
    ],
    'primary key' => ['leaf_workflow_id'],
  ];
  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function leaf_workflow_uninstall() {
  $conn = Database::getConnection();
  $table_name = 'leaf_workflow';

  if ($conn->schema()->tableExists($table_name)) {
    $conn->schema()->dropTable($table_name);
  }
}

/**
 * Add author by field into table.
 */
function leaf_workflow_update_91001() {
  $spec = [
    'type' => 'varchar',
    'description' => "Author By",
    'length' => 255,
    'not null' => FALSE,
  ];
  $schema = Database::getConnection()->schema();
  $schema->addField('leaf_workflow', 'author_by', $spec);
}

/**
 * Add long text field into table for media data.
 */
function leaf_workflow_update_91002() {
  $spec = [
    'type' => 'text',
    'size' => 'big',
    'not null' => FALSE,
    'description' => 'Media json string.',
  ];
  $schema = Database::getConnection()->schema();
  $schema->addField('leaf_workflow', 'media_json_value', $spec);
}
