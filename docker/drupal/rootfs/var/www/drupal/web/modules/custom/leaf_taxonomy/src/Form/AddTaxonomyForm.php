<?php

namespace Drupal\leaf_taxonomy\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Class AddTaxonomyForm.
 */
class AddTaxonomyForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ajax_add_taxonomy_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $voc_value = [];

    $field_name = \Drupal::routeMatch()->getParameter('field_name');

    $bundle = \Drupal::routeMatch()->getParameter('entity_bundle');

    $bundle_fields = \Drupal::getContainer()->get('entity_field.manager')->getFieldDefinitions('node', $bundle);
    $field_definition = $bundle_fields[$field_name];

    $label = $bundle_fields[$field_name]->getLabel();

    $settings = $field_definition->getSettings();

    if (isset($settings['handler_settings']['target_bundles'])) {
      $vocabulary_array = $settings['handler_settings']['target_bundles'];
      $voc_value = array_map("leaf_taxonomy_ucwords_texts", $vocabulary_array);
    }

    $form['taxonomy_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Taxonomy Label'),
      '#required' => TRUE,
    ];

    $form['leaf_sel_vocab'] = [
      '#title' => $this->t('Select Vocabulary'),
      '#type' => 'select',
      '#options' => $voc_value,
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#prefix' => '<div class="text-center">',
      '#type' => 'button',
      '#attributes' => ['class' => ['btn btn-success']],
      '#value' => $this->t('Save'),
      '#ajax' => [
        'callback' => '::validateTaxonomyAjax',
      ],
      '#suffix' => '<div class="contact-form-result-message"></div></div>',
    ];

    $form['contributor_markup'] = [
      '#type' => 'markup',
      '#markup' => '<p>Newly created ' . $label . ' will be available for selection.</p>',
    ];

    $form['#attached']['library'] = [
      'core/jquery',
      'core/drupal.ajax',
      'core/jquery.once',
      'core/jquery.form',
    ];

    // honeypot_add_form_protection($form, $form_state, array('honeypot', 'time_restriction'));.
    return $form;
  }

  /**
   * Ajax callback to validate / send.
   */
  public function validateTaxonomyAjax(array &$form, FormStateInterface $form_state) {

    // Initiate response.
    $response = new AjaxResponse();

    // Get vars.
    $taxonomy_name = Html::escape($form_state->getValue('taxonomy_label'));
    $vocabulary_name = $form_state->getValue('leaf_sel_vocab');

    /** SUCCESS */
    if (!empty(trim($taxonomy_name)) && !empty($vocabulary_name)) {

      $term_obj = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['name' => trim($taxonomy_name), 'vid' => $vocabulary_name]);
      $term_obj = reset($term_obj);

      if (!empty($term_obj)) {
        $message = $this->t('Term already exist.');
        $response->addCommand(new HtmlCommand('.contact-form-result-message', $message));
      }
      else {
        $new_term = Term::create([
          'name' => trim($taxonomy_name),
          'vid' => $vocabulary_name,
        ]);

        $new_term->enforceIsNew();
        $new_term->save();

        if ($new_term) {
          $message = $this->t("Contributor " . $taxonomy_name . " is created.");
          $response->addCommand(new HtmlCommand('.contact-form-result-message', $message));
          $command = new CloseModalDialogCommand();
          $response->addCommand($command);
        }
        else {
          $message = $this->t('Some technical issue.');
          $response->addCommand(new HtmlCommand('.contact-form-result-message', $message));
        }
      }
    }

    /** FIELD EMPTY */
    elseif (empty($taxonomy_name) || empty($vocabulary_name)) {
      $message = $this->t('Please fill out all fields.');
      $css = ['color' => 'red'];
      $response->addCommand(new CssCommand('.contact-form-result-message', $css));
      $response->addCommand(new HtmlCommand('.contact-form-result-message', $message));
    }

    \Drupal::messenger()->deleteAll();
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Nothing needed here, but mandatory by Interface.
  }

}
