<?php

namespace Drupal\leaf_baseline\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\bibcite\CitationStylerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Utility\Token;
use Drupal\Component\Utility\Xss;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;

/**
 * Provides a Citation Block.
 *
 * @Block(
 *   id = "citation_repository_item_block",
 *   admin_label = @Translation("Citation Repository Item Block"),
 *   category = @Translation("Citation Block"),
 * )
 */
class CitationRepositoryItemBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $bibcite_settings = \Drupal::config('bibcite.settings');
    $citation_style = $bibcite_settings->get('default_style');
    $response = '';
    $citation_styler = \Drupal::service('bibcite.citation_styler');
    $citation_styler->setStyleById($citation_style);

    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof \Drupal\node\NodeInterface) {
      $nid = $node->id();
    }
    elseif(is_numeric($node)) {
      $nid = $node;
    }
    $langcode = $citation_styler->getLanguageCode();
    $data = \Drupal::service('citation_select.citation_processor')->getCitationArray($nid, $langcode);
    $this->sanitizeArray($data);
    $citation_response = $citation_styler->render($data);

    // Get media thumbnail
    $mid = \Drupal::entityQuery('media')
      ->condition('bundle', 'image')
      ->condition('field_media_of', $nid)
      ->accessCheck(TRUE)
      ->execute();

    $url = '';
    if (!empty($mid)) {
      $mid = reset($mid);
      $image_style = ImageStyle::load('thumbnail_islandora_image');
      $media = Media::load($mid);
      $fid = $media->field_media_image->target_id;
      $file = File::load($fid);
      $image_uri = $file->getFileUri();
      if ($image_style != NULL && $file->uri->value != NULL) {
        $destination_uri = $image_style->buildUri($file->uri->value);
        $image_style->createDerivative($image_uri, $destination_uri);
        $url = \Drupal::service('file_url_generator')->generateAbsoluteString($destination_uri);
      }
    }

    return [
      '#theme' => 'citation_block',
      '#thumbnail_image' => $url,
      '#citation_text' => $citation_response
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * Recursively sanitizes all elements of array.
   *
   * @param array $data
   *   Array to sanitize.
   */
  protected function sanitizeArray(array &$data) {
    foreach ($data as $delta => $item) {
      if (is_array($item)) {
        $this->sanitizeArray($item);
      }
      else {
        $data[$delta] = Xss::filter($item);
      }
    }
  }

}
