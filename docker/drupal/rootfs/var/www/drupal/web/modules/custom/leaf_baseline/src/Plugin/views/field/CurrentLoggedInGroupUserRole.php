<?php

namespace Drupal\leaf_baseline\Plugin\views\field;

use Drupal\Core\Render\Markup;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;

/**
 * Current logged in group user role field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("current_logged_in_group_user_role_field")
 */
class CurrentLoggedInGroupUserRole extends FieldPluginBase {

  /**
   * The current display.
   *
   * @var string
   *   The current display of the view.
   */
  protected $currentDisplay;

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->currentDisplay = $view->current_display;
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    if (\Drupal::currentUser()->isAnonymous()) {
      return;
    }

    $current_user = \Drupal::currentUser();
    $current_user_roles = $current_user->getRoles();
    // Check user has specific role; example 'administrator' role
    if (in_array('administrator', $current_user_roles)) {
       return "admin";
    }

    $gid = $values->groups_field_data_group_content_field_data_id;
    $current_group = \Drupal::entityTypeManager()->getStorage('group')->load($gid);
    // Get group members.
    $members = $current_group->getMembers();
    foreach ($members as $member) {
      $user = $member->getUser();
      $userids[] = $user->id();
      $memberuser[$user->id()] = $member;
    }

    // Check current active user group member/editor.
    $loggedin_group_user_role = "authenticated";

    if (!empty($userids)) {
      if (in_array(\Drupal::currentUser()->id(), $userids)) {
        $member = $memberuser[\Drupal::currentUser()->id()];
        $member_object = $member->getGroupContent();
        $member_roles = $member_object->get('group_roles')->getValue();
        if (!empty($member_roles)) {
          foreach ($member_roles as $member_role) {
            $m_roles[] = $member_role["target_id"];
          }
  
          if (in_array("project-editor", $m_roles)) {
            $loggedin_group_user_role = 'project_editor';
          } else {
            $loggedin_group_user_role = 'project_member';
          }
        } else {
          $loggedin_group_user_role = 'project_member';
        }
      }
    }

    return $loggedin_group_user_role;
  }

}
