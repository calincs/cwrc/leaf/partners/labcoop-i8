/**
 * @file
 * Initialize the leaf dtoc library.
 */

(($, Drupal, once, drupalSettings) => {
  /**
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.leafdtocEditor = {
    attach: function (context) {
      var pathname = window.location.pathname;
      var nid = pathname.split('/')[2];

      const dtocConfig = sessionStorage.getItem("dtocInputConfig" + nid);
      var datas = JSON.parse(dtocConfig);

      if (jQuery.isEmptyObject(datas)) {

      } else {
        document.querySelector('input[name="part"]').value = datas.documents;
        document.querySelector('input[name="title"]').value = datas.documentTitle;
        document.querySelector('input[name="part_content"]').value = datas.documentContent;
        document.querySelector('input[name="part_author"]').value = datas.documentAuthor;
        document.querySelector('input[name="index"]').value = datas.indexDocument;
        document.querySelector('input[name="figures"]').value = datas.documentImages;
        document.querySelector('input[name="notes"]').value = datas.documentNotes;
        document.querySelector('input[name="references"]').value = datas.documentLinks;
        document.querySelector('textarea[name="markup_index"]').value = JSON.stringify(datas.curation.markup);
        document.querySelector('input[name="toc"]').value = JSON.stringify(datas.curation.toc);

        $('input[name="simplify_config"]').prop('checked', datas.ignoreNamespace);
      }

      $('.selectall.form-checkbox').on('click',function(){
        if(this.checked){
          $(this).parent().parent().parent().parent().next('#collecion-xml-files-options').find('input').attr('checked','checked');
        } else {
          $(this).parent().parent().parent().parent().next('#collecion-xml-files-options').find('input').removeAttr('checked');
        }
      });
    }
  };
})(jQuery, Drupal, once, drupalSettings);

