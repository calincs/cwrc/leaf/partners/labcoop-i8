<?php

namespace Drupal\leaf_baseline\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;


/**
 * Class LeafBaselineProfileSwitcherForm.
 *
 * @package Drupal\leaf_baseline\Form
 */
class LeafBaselineProfileSwitcherForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'leaf_baseline_profile_switcher';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm($form, FormStateInterface $form_state,) {
    $form['new_profile'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('New profile'),
        '#required' => 'required',
        '#default_value' => 'minimal',
        '#description' => $this->t('Add Machine name for new profile to which you want to switch.'),
      );
    $form['submit'] = [
        '#type' => 'submit',
        '#value' => 'Submit',
    ];  
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
    $current_profile = $form_state->getValue('current_profile');
    $new_profile = $form_state->getValue('new_profile');
    // Call profile switcher service.
    $switcher = \Drupal::service('profile_switcher.profile_switcher');
    $switcher->switchProfile($new_profile);
    \Drupal::messenger()->addMessage('Profile is switched successfully.');
  }

}
