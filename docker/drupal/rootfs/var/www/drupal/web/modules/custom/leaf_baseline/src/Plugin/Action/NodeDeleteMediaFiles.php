<?php

namespace Drupal\leaf_baseline\Plugin\Action;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;

/**
 * Unpublish node action.
 *
 * @Action(
 *   id = "node_delete_media_files",
 *   label = @Translation("Delete associated media and files"),
 *   type = "node"
 * )
 */
class NodeDeleteMediaFiles extends ViewsBulkOperationsActionBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    if ($entity->getEntityTypeId() == "node") {
      $mediaStorage = \Drupal::entityTypeManager()->getStorage('media');
      $mids = $mediaStorage->getQuery()
        ->accessCheck(FALSE)
        ->condition('field_media_of', $entity->id())
        ->sort('created', 'DESC')
        ->execute();

      if (!empty($mids)) {
        $media_entities = $mediaStorage->loadMultiple($mids);
        $mediaStorage->delete($media_entities);

        return $this->t($entity->getTitle() . " - associated media are deleted.");
      }
      else {
        return $this->t($entity->getTitle() . " - no media found.");
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    /** @var \Drupal\node\NodeInterface $object */
    $result = $object->access('update', $account, TRUE);

    return $return_as_object ? $result : $result->isAllowed();
  }

}
