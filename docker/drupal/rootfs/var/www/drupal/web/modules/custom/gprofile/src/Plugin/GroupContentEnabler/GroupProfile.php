<?php

namespace Drupal\gprofile\Plugin\GroupContentEnabler;

use Drupal\Core\Entity\EntityInterface;
use Drupal\group\Plugin\GroupContentEnablerBase;
use Drupal\profile\Entity\ProfileType;
use Drupal\profile\Entity\ProfileTypeInterface;


/**
 * @GroupContentEnabler(
 *  id = "group_profile",
 *  label = @Translation("Group profile"),
 *  description = @Translation("Add profiles to groups both publicly and privately"),
 *  entity_type_id = "profile",
 *  entity_access = TRUE,
 *  pretty_path_key = "profile",
 *  reference_label = @Translation("Title"),
 *  reference_description = @Translation("The title of the profile to add to the group"),
 *  deriver = "Drupal\gprofile\Plugin\GroupContentEnabler\GroupProfileDeriver",
 *  handlers = {
 *     "permission_provider" = "Drupal\group\Plugin\GroupContentPermissionProvider",
 *  }
 * )
 */
class GroupProfile extends GroupContentEnablerBase {

  /**
   * Retrieves the group type this plugin supports.
   *
   * @return EntityInterface|ProfileTypeInterface
   *   The group type this plugin supports.
   */
  protected function getProfileType() {
    return ProfileType::load($this->getEntityBundle());
  }
}
