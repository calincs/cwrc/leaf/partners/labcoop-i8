<?php

namespace Drupal\leaf_dtoc\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\TermInterface;

/**
 * DToC access permission.
 */
class AccessController {

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(NodeInterface $node) {
    // If node is repository item.
    if ($node->bundle() == 'islandora_object') {
      if (isset($node->get('field_resource_type')->entity)) {
        $resource_type = $node->get('field_resource_type')->entity->getName();
        // If resource type is Text.
        if ($resource_type == 'Text') {
          $model = $node->get('field_model')->entity->getName();
          // If model type is Digital Document.
          if ($model == 'Digital Document') {
            // If display hint is selected as none.
            $display_hint = $node->field_display_hints->entity;
            if ($display_hint instanceof TermInterface) {
              $field_display_hint_value = $node->get('field_display_hints')->entity->getName();
              if ($field_display_hint_value == 'DToC') {
                return AccessResult::allowed();
              }
            }
          }
        }
      }
    }
    return AccessResult::forbidden();
  }

}
