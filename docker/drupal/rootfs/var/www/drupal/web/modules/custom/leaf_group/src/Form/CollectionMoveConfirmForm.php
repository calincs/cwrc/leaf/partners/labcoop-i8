<?php

namespace Drupal\leaf_group\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Collection move confirmation form.
 */
class CollectionMoveConfirmForm extends ConfirmFormBase {

  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $id;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $id = NULL, $group_id = NULL) {
    $this->id = $id;
    $this->group_id = $group_id;

    $collection_node = \Drupal::entityTypeManager()->getStorage('node')->load($this->id);
    $collection_title = $collection_node->getTitle();

    $current_group = \Drupal::entityTypeManager()->getStorage('group')->load($this->group_id);
    $current_group_title = $current_group->label();

    $group_contents = \Drupal::entityTypeManager()
      ->getStorage('group_content')
      ->loadByProperties([
        'entity_id' => $this->id,
      ]);

    foreach ($group_contents as $pa_group_content) {
      $pa_group = $pa_group_content->get('gid')->referencedEntities();
      $pa_group_obj = reset($pa_group);
      $parent_group_title = $pa_group_obj->label();
    }

    $current_route_name = \Drupal::routeMatch()->getRouteName();

    $markup_text = $this->t('<h4 class="confirmation-title">You are about to move collection <strong><em>' . $collection_title . '</em></strong> and all its descendants from Project <strong><em>' . $parent_group_title . '</em></strong> to Project <strong><em>' . $current_group_title . '</em></strong>. Please confirm.</h4>');
    $form['form_markup']['#markup'] = $markup_text;

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $repository_item_nid = $this->id;
    $group_id = $this->group_id;

    $group = \Drupal::entityTypeManager()->getStorage('group')->load($group_id);
    $current_group_title = $group->label();
    $node = \Drupal::entityTypeManager()->getStorage('node')->load($repository_item_nid);
    $collection_title = $node->getTitle();
    $group_contents = \Drupal::entityTypeManager()
      ->getStorage('group_content')
      ->loadByProperties([
        'entity_id' => $repository_item_nid,
      ]);

    $group_role = $editor_user_ids = $curr_editor_user_ids = [];
    $current_user_id = \Drupal::currentUser()->id();
    $current_u_roles = \Drupal::currentUser()->getRoles();

    foreach ($group_contents as $pa_group_content) {
      $pa_group = $pa_group_content->get('gid')->referencedEntities();
      $pa_group_obj = reset($pa_group);
      $pa_group_id = $pa_group_obj->id();
      $group_members = $pa_group_obj->getMembers();

      foreach ($group_members as $group_member) {
        $old_group_content = $group_member->getGroupContent();
        if ($old_group_content->hasField('group_roles')) {
          $group_role_arr = $old_group_content->get('group_roles')->getValue();
          $group_user = $group_member->getUser();
          if (!empty($group_role_arr)) {
            foreach ($group_role_arr as $group_role_val) {
              $group_role[$group_user->id()] = $group_role_val['target_id'];
            }

            // Get editor user id of respective group members.
            if (!empty($group_role[$current_user_id]) && $group_role[$current_user_id] == 'project-editor') {
              $editor_user_ids[] = $group_user->id();
            }
          }
        }
      }
    }

    $current_group_members = $group->getMembers();

    foreach ($current_group_members as $current_group_member) {
      $curr_old_group_content = $current_group_member->getGroupContent();
      if ($curr_old_group_content->hasField('group_roles')) {
        $curr_group_role_arr = $curr_old_group_content->get('group_roles')->getValue();
        $curr_group_user = $current_group_member->getUser();
        if (!empty($curr_group_role_arr)) {
          foreach ($curr_group_role_arr as $curr_group_role_val) {
            $curr_group_role[$curr_group_user->id()] = $curr_group_role_val['target_id'];
          }

          // Get editor user id of respective group members.
          if (!empty($curr_group_role[$current_user_id]) && $curr_group_role[$current_user_id] == 'project-editor') {
            $curr_editor_user_ids[] = $curr_group_user->id();
          }
        }
      }
    }

    // If current user is assigned to both project and it has editor role
    // in group members,
    // Remove group nodes from another proejct and add into new project.
    if ((in_array('administrator', $current_u_roles)) || (in_array($current_user_id, $editor_user_ids) && in_array($current_user_id, $curr_editor_user_ids))) {

      $child_nodes_ids = [];
      // Add parent content to group.
      $group->addContent($node, 'group_node:islandora_object');
      $child_nodes_ids = leaf_group_add_child_nodes_group($repository_item_nid, $group_id);

      // Get child group nodes.
      $child_group_nodes = \Drupal::entityTypeManager()
        ->getStorage('node')
        ->loadByProperties([
          'field_member_of' => $repository_item_nid,
          'type' => 'islandora_object',
        ]);

      foreach ($child_group_nodes as $child_nid => $child_group_node) {
        // Get child group contents.
        $child_group_contents = \Drupal::entityTypeManager()
          ->getStorage('group_content')
          ->loadByProperties([
            'entity_id' => $child_nid,
          ]);

        // Remove child group nodes from another project..
        foreach ($child_group_contents as $child_group_content) {
          $child_group_content->delete();
        }
      }

      $pa_child_nodes_ids = [];
      // Add parent content to group.
      $pa_child_nodes_ids = leaf_group_get_child_nodes_group($repository_item_nid, $pa_group_id);

      do {
        if (!empty($pa_child_nodes_ids)) {
          $all_child_empty_check = [];
          foreach ($pa_child_nodes_ids as $nid) {
            $pa_child_nodes_ids = leaf_group_get_child_nodes_group($nid, $group_id);
            $all_child_empty_check[] = $pa_child_nodes_ids;

            // Get child group nodes.
            $child_group_nodes = \Drupal::entityTypeManager()
              ->getStorage('node')
              ->loadByProperties([
                'field_member_of' => $nid,
                'type' => 'islandora_object',
              ]);

            foreach ($child_group_nodes as $child_nid => $child_group_node) {
              // Get child group contents.
              $child_group_contents = \Drupal::entityTypeManager()
                ->getStorage('group_content')
                ->loadByProperties([
                  'entity_id' => $child_nid,
                ]);

              // Remove child group nodes from another project..
              foreach ($child_group_contents as $child_group_content) {
                $child_group_content->delete();
              }
            }
          }
          $pa_child_nodes_ids = leaf_group_array_flatten($all_child_empty_check);
        }
      } while (!empty($pa_child_nodes_ids));

      // Remove parent group content from another project.
      foreach ($group_contents as $group_content) {
        $group_content->delete();
      }

      // Add all children nodes of selected repositery item.
      $child_nodes_ids = [];
      // Add parent content to group.
      $group->addContent($node, 'group_node:islandora_object');
      $child_nodes_ids = leaf_group_add_child_nodes_group($repository_item_nid, $group_id);

      do {
        if (!empty($child_nodes_ids)) {
          $all_child_empty_check = [];
          foreach ($child_nodes_ids as $nid) {
            $child_nodes_ids = leaf_group_add_child_nodes_group($nid, $group_id);
            $all_child_empty_check[] = $child_nodes_ids;
          }
          $child_nodes_ids = leaf_group_array_flatten($all_child_empty_check);
        }
      } while (!empty($child_nodes_ids));

      $message = $this->t("Collection %collection_title has been moved to Project %current_group_title.", ['%collection_title' => $collection_title, '%current_group_title' => $current_group_title]);
      \Drupal::messenger()->addStatus($message);
    }
    else {
      $collection_node = \Drupal::entityTypeManager()->getStorage('node')->load($this->id);
      $collection_title = $collection_node->getTitle();

      $current_group = \Drupal::entityTypeManager()->getStorage('group')->load($this->group_id);
      $current_group_title = $current_group->label();

      $group_contents = \Drupal::entityTypeManager()
        ->getStorage('group_content')
        ->loadByProperties([
          'entity_id' => $this->id,
        ]);

      foreach ($group_contents as $pa_group_content) {
        $pa_group = $pa_group_content->get('gid')->referencedEntities();
        $pa_group_obj = reset($pa_group);
        $parent_group_title = $pa_group_obj->label();
      }
      // Show message if they don't have editor privileges on the other project.
      $message = $this->t("Collection %collection_title already belongs to Project %parent_group_title. Please contact Project %parent_group_title team if you think it should belong to Project %current_group_title", ['%collection_title' => $collection_title, '%parent_group_title' => $parent_group_title, '%current_group_title' => $current_group_title]);
      \Drupal::messenger()->addWarning($message);
    }
    $form_state->setRedirect('view.group_nodes.page_1', ['group' => $group_id]);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "project_move_confirm_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.group_content.add_form', ['plugin_id' => 'group_node:islandora_object', 'group' => $this->group_id]);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $collection_node = \Drupal::entityTypeManager()->getStorage('node')->load($this->id);
    $collection_title = $collection_node->getTitle();

    $current_group = \Drupal::entityTypeManager()->getStorage('group')->load($this->group_id);
    $current_group_title = $current_group->label();

    $group_contents = \Drupal::entityTypeManager()
      ->getStorage('group_content')
      ->loadByProperties([
        'entity_id' => $this->id,
      ]);

    foreach ($group_contents as $pa_group_content) {
      $pa_group = $pa_group_content->get('gid')->referencedEntities();
      $pa_group_obj = reset($pa_group);
      $parent_group_title = $pa_group_obj->label();
    }

    return $this->t('You are about to move collection %collection_title and all its descendants from Project %parent_group_title to Project %current_group_title. Please confirm.', ['%collection_title' => $collection_title, '%parent_group_title' => $parent_group_title, '%current_group_title' => $current_group_title]);
  }

}
