<?php

namespace Drupal\leaf_workflow\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupContent;
use Drupal\user\Entity\User;
use Drupal\Core\Entity\EntityInterface;
use Drupal\private_message\Entity\PrivateMessage;
use Drupal\node\Entity\Node;

/**
 * Implements a leaf workflow form.
 */
class LeafWorkflow extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'leaf_workflow_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $current_path = \Drupal::service('path.current')->getPath();
    $path_args = explode('/', $current_path);
    $about_page = FALSE;

    // Get the form values and raw input (unvalidated values).
    $values = $form_state->getValues();
    // Define a wrapper id to populate new content into.
    $ajax_wrapper = 'workflow-ajax-wrapper';

    if (!empty($path_args)) {
      if ($path_args[1] == 'node' && is_numeric($path_args[2]) && $path_args[3] == 'about') {
        $about_page = TRUE;
      }
    }

    // If it's not about page no need to load form.
    if ($about_page == FALSE) {
      return;
    }

    $node_id = $path_args[2];
    $group_ids = [];
    $group_ids = \Drupal::entityQuery('group_content')
      ->accessCheck(TRUE)
      ->condition('entity_id', $node_id)
      ->execute();

    // Check whether node is assigned to any group or not.
    $public_visibility_label = t('Make visible to the public (if not checked, this object will only be visible to site administrators).');
    if (!empty($group_ids)) {
      $relations = GroupContent::loadMultiple($group_ids);
      foreach ($relations as $rel) {
        if ($rel->getEntity()->getEntityTypeId() == 'node') {
          $group_label = $rel->getGroup()->label();
          $public_visibility_label = "Make visible to the public (if not checked, this object will only be visible to site administrators and members of {$group_label}).";
        }
      }
    }

    $form['form_info'] = [
      '#markup' => '<h6>NEW WORKFLOW RECORD</h6><br>',
    ];

    // Get record from table to set default value.
    $latest_vid = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->getLatestRevisionId($node_id);
    // Load the revision.
    $last_revision = \Drupal::entityTypeManager()->getStorage('node')->loadRevision($latest_vid);
    // Get the revisions moderation state.
    $last_revision_state = $last_revision->get('moderation_state')->getString();

    $workflow_query_field_array = [
      'activity',
      'public_visibility',
      'is_activity_completed',
      'moderation_state',
      'assignee',
    ];
    $result = leaf_workflow_get_last_revision_data($node_id, $workflow_query_field_array);

    $user = User::load(\Drupal::currentUser()->id());
    $name = $user->get('name')->value;
    $user_name = !empty($result['assignee']) ? $result['assignee'] : $name;
    $form['current_user'] = [
      '#markup' => '<span class="label">User:</span><span class="value">' . $user_name . '</span>',
    ];

    $current_time = new DrupalDateTime('now', 'UTC');
    $form['current_date'] = [
      '#markup' => '<span class="label">Date/time:</span><span class="value">' . $current_time . '</span>',
    ];

    // Change stage options based on group role.
    $stage_options = $this->defaultStagesOptions();

    $stage_options_static_array = [
      'In Progress' => 'draft',
      'Done pending action' => 'done_pending_action',
      'Peer-Reviewed' => 'peer_reviewed',
      'Released' => 'published',
      'Retracted' => 'retracted',
    ];

    $form['stage_options'] = [
      '#title' => t('Stage:'),
      '#type' => 'select',
      '#default_value' => !empty($result['moderation_state']) ? $stage_options_static_array[$result['moderation_state']] : 'draft',
      '#options' => $stage_options,
      '#ajax' => [
        'callback' => [$this, 'mySelectChange'],
        'event' => 'change',
        'wrapper' => $ajax_wrapper,
      ],
    ];

    $activity_options = [
      'checked' => t('Checked'),
      'edited' => t('Edited'),
      'created' => t('Created'),
      'deposited' => t('Deposited'),
      'machine_processed' => t('Machine Processed'),
      'reviewed' => t('Reviewed'),
      'peer_reviewed' => t('Peer Reviewed'),
      'published' => t('Published'),
      'retracted' => t('Retracted'),
      'published_version_revised' => t('Published version revised'),
    ];

    $activity_options_draft = [
      'checked' => t('Checked'),
      'edited' => t('Edited'),
      'created' => t('Created'),
      'deposited' => t('Deposited'),
      'machine_processed' => t('Machine Processed'),
      'reviewed' => t('Reviewed'),
      'published_version_revised' => t('Published version revised'),
    ];

    $activity_options_published = [
      'checked' => t('Checked'),
      'edited' => t('Edited'),
      'machine_processed' => t('Machine Processed'),
      'reviewed' => t('Reviewed'),
    ];

    $activity_options_done_pending_action = [
      'checked' => t('Checked'),
      'edited' => t('Edited'),
      'machine_processed' => t('Machine Processed'),
      'reviewed' => t('Reviewed'),
    ];

    $activity_options_peer_reviewed = [
      'peer_reviewed' => t('Peer Reviewed'),
    ];

    $activity_options_retracted = [
      'retracted' => t('Retracted'),
    ];

    $languageCode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $storage = \Drupal::entityTypeManager()->getStorage('node');
    $revision_id = $storage->getLatestTranslationAffectedRevisionId($node_id, $languageCode);
    $revision = $storage->loadRevision($revision_id);

    if (!empty($revision)) {
      $mod = $revision->get('moderation_state')->getValue();
    }

    $selected_stage_val = 'draft';
    // if (!empty($mod) && !empty($mod[0])) {
    //   $selected_stage_val = $mod[0]['value'];
    // }
    if (!empty($result['moderation_state'])) {
      $selected_stage_val = $stage_options_static_array[$result['moderation_state']];
    }

    if (!empty($values) && !empty($values['stage_options'])) {
      $selected_stage_val = $values['stage_options'];
    }

    // Set default public visibility.
    $default_public_visibility = FALSE;
    if (!empty($result) && $result['public_visibility'] == 'Public') {
      $default_public_visibility = TRUE;
    }

    if (!empty($selected_stage_val) && $selected_stage_val == 'published') {
      $default_public_visibility = TRUE;
    }

    $form['public_visibility'] = [
      '#type' => 'checkbox',
      '#title' => $public_visibility_label,
      '#default_value' => $default_public_visibility,
    ];

    switch ($selected_stage_val) {
      case 'draft':
        $activity_options = $activity_options_draft;
        break;

      case 'published':
        $activity_options = $activity_options_published;
        break;

      case 'done_pending_action':
        $activity_options = $activity_options_done_pending_action;
        break;

      case 'peer_reviewed':
        $activity_options = $activity_options_peer_reviewed;
        break;

      case 'retracted':
        $activity_options = $activity_options_retracted;
        break;
    }

    // Build a wrapper for the ajax response.
    $form['workflow_ajax_container'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => $ajax_wrapper,
      ],
    ];

    $form['workflow_ajax_container']['activity_options'] = [
      '#title' => t('Activity:'),
      '#type' => 'select',
      '#default_value' => !empty($result) ? str_replace(' ', '_', strtolower($result['activity'] ?? '')) : 'edited',
      '#options' => $activity_options,
    ];

    $form['is_activity_completed'] = [
      '#type' => 'radios',
      '#title' => t('Is the activity complete?'),
      '#options' => ['1' => 'Yes', '0' => 'No'],
      '#default_value' => !empty($result) ? ($result['is_activity_completed'] == 'Complete' ? '1' : '0') : '0',
    ];

    $form['description'] = [
      '#type' => 'textfield',
      '#title' => t('Description (optional/public)'),
      '#required' => FALSE,
      '#maxlength' => 50,
      '#default_value' => '',
    ];

    $form['assign_to'] = [
      '#title' => ('Assign to:'),
      '#type' => 'entity_autocomplete',
      '#target_type' => 'user',
      '#required' => FALSE,
      '#selection_settings' => [
        'include_anonymous' => FALSE,
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('SAVE'),
      '#attributes' => [
        'class' => ['about-primary-button'],
      ],
    ];

    $form['cancel'] = [
      '#type' => 'button',
      '#value' => t('CANCEL'),
      '#button_type' => 'primary',
      '#attributes' => [
        'class' => ['about-secondary-button', 'cancel-workflow-record'],
      ],
    ];

    $form['#theme'] = 'leaf_workflow_form';
    $form['#attached']['library'][] = 'leaf_workflow/confirmation-popup';
    return $form;
  }

  /**
   * The callback function for when the `my_select` element is changed.
   *
   * What this returns will be replace the wrapper provided.
   */
  public function mySelectChange(array $form, FormStateInterface $form_state) {
    // Return the element that will replace the wrapper (we return itself).
    return $form['workflow_ajax_container'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $current_path = \Drupal::service('path.current')->getPath();
    $path_args = explode('/', $current_path);
    $field = $form_state->getValues();

    if (!empty($path_args)) {
      if ($path_args[1] == 'node' && is_numeric($path_args[2])) {
        $node_id = $path_args[2];

        // Create a private message and send notification to assignee.
        try {
          $result = leaf_workflow_get_last_revision_data($node_id, ['assignee']);
          $last_assignee_name = $result['assignee'];

          if (!empty($field['assign_to'])) {
            $to_user = User::load($field['assign_to']);
            $to_user_name = $to_user->get('name')->value;

            // Only send message if user first time gets assigned.
            if (strcmp($last_assignee_name, $to_user_name) !== 0) {
              $object_details = Node::load($node_id);
              $object_name = $object_details->getTitle();
              $from_user = User::load(\Drupal::currentUser()->id());

              $members = [$from_user, $to_user];
              $service = \Drupal::service('private_message.service');
  
              // This will create a thread if one does not exist.  
              $private_message_thread = $service->getThreadForMembers($members);
            
              // Add a Message to the thread.
              $private_message = PrivateMessage::create();
              $private_message->set('owner', $from_user);
              $private_message->set('message', "{$object_name} is assigned to you.");
              $private_message->save();
              $private_message_thread->addMessage($private_message)->save();
            }
          }
        }
        catch (Exception $e) {
          \Drupal::logger('leaf_workflow')->error($e->getMessage());
        }

        // Create entry into node core revision table.
        leaf_workflow_create_new_revision($node_id, $field['description'], $field['stage_options'], $form_state);

        // Create entry into custom workflow table.
        leaf_workflow_insert_record_into_table($node_id, $field['description'], FALSE, $form_state, $form);

        $tags = ['node:' . $path_args[2]];
        Cache::invalidateTags($tags);
        \Drupal::entityTypeManager()->getStorage('node')->resetCache([$path_args[2]]);
      }
    }
  }

  /**
   * Provides default workflow stage options.
   */
  private function defaultStagesOptions() {
    $current_path = \Drupal::service('path.current')->getPath();
    $path_explode = explode('/', $current_path);
    $nid = $path_explode[2] ?? '';

    $stage_options = [
      'draft' => t('In Progress'),
      'published' => t('Released'),
      'done_pending_action' => t('Done pending action'),
      'peer_reviewed' => t('Peer-Reviewed'),
      'retracted' => t('Retracted'),
    ];

    if ($path_explode[1] == 'node' && is_numeric($nid)) {
      $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid);

      if ($node) {
        $user_has_access = FALSE;
        $user_is_admin = FALSE;
        $group_content = GroupContent::loadByEntity($node);
        $user = User::load(\Drupal::currentUser()->id());
        $roles = $user->getRoles();

        if (!empty($group_content)) {
          $group_content = reset($group_content);
          $group = $group_content->getGroup();
          // Check if current user is a group member.
          $user_member = $group->getMember($user);
          if ($user_member) {
            $user_member_roles = $user_member->getRoles();
            $user_member_role_keys = array_keys($user_member_roles);

            if (in_array('project-editor', $user_member_role_keys)) {
              $stage_options = [
                'draft' => t('In Progress'),
                'done_pending_action' => t('Done pending action'),
                'published' => t('Released'),
                'retracted' => t('Retracted'),
              ];
            }

            if (in_array('project-contributor', $user_member_role_keys)) {
              $stage_options = [
                'draft' => t('In Progress'),
                'done_pending_action' => t('Done pending action'),
              ];
            }

            if (in_array('project-peer_reviewer', $user_member_role_keys)) {
              $stage_options = [
                'peer_reviewed' => t('Peer-Reviewed'),
              ];
            }
          }
        }
      }

      return $stage_options;
    }
  }

}
