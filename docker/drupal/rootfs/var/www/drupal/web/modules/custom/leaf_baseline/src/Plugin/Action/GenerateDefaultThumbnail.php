<?php

namespace Drupal\leaf_baseline\Plugin\Action;

use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Action description.
 *
 * @Action(
 *   id = "wap_baseline_generate_default_thumbnail",
 *   label = @Translation("Default Thumbnail - Generate a default thumbnail based on resource type and model"),
 *   type = ""
 * )
 */
class GenerateDefaultThumbnail extends ViewsBulkOperationsActionBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    // Do some processing..
    if ($entity->bundle() == 'islandora_object') {
      $entity = leaf_repository_generate_default_thumbnail($entity);
      $entity->save();
      return $this->t('Generated default thumbnail for repository items.');
    }
    else {
      return $this->t('Default thumbnails can only be generated for repository item node types.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    // If certain fields are updated, access should be checked against them as well.
    // @see Drupal\Core\Field\FieldUpdateActionBase::access().
    return $object->access('update', $account, $return_as_object);
  }

}