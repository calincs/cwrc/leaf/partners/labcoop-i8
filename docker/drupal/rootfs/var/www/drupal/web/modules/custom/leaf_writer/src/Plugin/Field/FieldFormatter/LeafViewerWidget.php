<?php

namespace Drupal\leaf_writer\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\user\Entity\User;

/**
 * Plugin implementation of the
 *    'LeafViewerWidget' formatter.
 *
 * @FieldFormatter(
 *   id = "leaf_viewer",
 *   label = @Translation("Leaf Writer Read Only"),
 *   field_types = {
 *     "file",
 *     "media"
 *   }
 * )
 */
class LeafViewerWidget extends FormatterBase {

  /**
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   * @param $langcode
   * @return array
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $supported_mime_types = ['application/xml', 'text/xml'];

    foreach ($items as $delta => $item) {
      $main_property = $item->getFieldDefinition()->getFieldStorageDefinition()->getMainPropertyName();
      $value = $item->{$main_property};
      if ($item->entity->getEntityTypeId() == 'file') {
        $itemList = $item->getValue();
        if (!empty($itemList)) {
          $file_id = $itemList['target_id'];
        }
      }
      else {
        $media = \Drupal::entityTypeManager()->getStorage('media')->load($value);
        if ($media == NULL) {
          return $elements;
        }
        $file_id = $media->hasField('field_media_document') ? $media->field_media_document->getValue()[0]['target_id'] : '';
      }

      // View media revision on node revision page.
      $route_match = \Drupal::routeMatch();
      if ($route_match->getRouteName() == "entity.node.revision") {
        $current_path = \Drupal::service('path.current')->getPath();
        $path_args = explode('/', $current_path);
        $nid = $path_args[2];
        $rev_id = $path_args[4];

        $result = \Drupal::database()->select('leaf_workflow', 'c')
          ->fields('c', ['media_json_value'])
          ->condition('c.revision_id', $rev_id, '=')
          ->execute()
          ->fetchAssoc();

        if (!empty($result)) {
          $media_array = json_decode($result['media_json_value'], TRUE);
          $languageCode = \Drupal::languageManager()->getCurrentLanguage()->getId();

          foreach ($media_array as $media_id => $media_rev_id) {
            $media = \Drupal::entityTypeManager()->getStorage('media')->loadRevision($media_rev_id);
            $file_id = $media->hasField('field_media_document') ? $media->field_media_document->getValue()[0]['target_id'] : '';
          }
        }
      }

      $file = !empty($file_id) ? File::load($file_id) : [];
      if (!$file || !($file instanceof FileInterface)) {
        return $elements;
        // Throw new \InvalidArgumentException('Invalid file argument provided for #file parameter.');.
      }

      if (!in_array($file->getMimeType(), $supported_mime_types)) {
        return $elements;
        // Throw new \InvalidArgumentException('File not supported for leaf viewer.');.
      }

      // Set the leaf writer read only data attributes.
      $host = \Drupal::request()->getSchemeAndHttpHost();
      $uri = $file->getFileUri();

      $full_path = \Drupal::service('file_url_generator')->generateString($uri);
      $user = User::load(\Drupal::currentUser()->id());
      $orcid_id = $user->field_orcid_id->value ?? '';
      $elements[$delta] = [
        '#theme' => 'leaf_writer_view_formatter',
        '#url' => $uri,
        '#xml_content' => trim(file_get_contents($uri)),
        '#data_leaf_writer_config' => Json::encode([
          'xml_file_url' => $host . $full_path,
          'user' => [
            'name' => $user->getDisplayName(),
            'id' => $orcid_id ? 'https://orcid.org/' . $orcid_id : $user->id(),
          ],
        ]),
      ];
    }
    $elements['#attached']['library'][] = 'leaf_writer/initializer';
    $elements['#attached']['drupalSettings']['leafWriter']['readOnly'] = TRUE;
    $elements['#cache']['max-age'] = 0;

    return $elements;
  }

}
