# Common to all services
x-common: &common
  restart: "no"
  secrets:
    - source: CERT_PUBLIC_KEY
    - source: CERT_PRIVATE_KEY
    - source: CERT_AUTHORITY
    - source: UID
    - source: GID

# Common build settings
x-build-common: &build-common
  args:
    REPOSITORY: ${ISLE_REPOSITORY}
    TAG: ${ISLE_TAG}
  x-bake:
    platforms:
      - linux/amd64
      - linux/arm64

x-traefik-https-redirect-middleware: &traefik-https-redirect-middleware
  traefik.enable: true
  traefik.http.middlewares.https-redirect.redirectscheme.permanent: true
  traefik.http.middlewares.https-redirect.redirectscheme.scheme: https

x-traefik-https-redirect: &traefik-https-redirect https-redirect

networks:
  default:

volumes:
  activemq-data: {}
  cantaloupe-data: {}
  code-server-data: {}
  drupal-private-files: {}
  drupal-public-files: {}
  drupal-root: {}
  jetbrains-cache: { external: true }
  jetbrains-config: { external: true }
  mariadb-data: {}
  matomo-config-data: {}
  solr-data: {}

secrets:
  CERT_PUBLIC_KEY:
    file: ./build/certs/cert.pem
  CERT_PRIVATE_KEY:
    file: ./build/certs/privkey.pem
  CERT_AUTHORITY:
    file: ./build/certs/rootCA.pem
  UID:
    file: ./build/secrets/UID
  GID:
    file: ./build/secrets/GID

services:
  alpaca:
    <<: *common
    image: ${ISLE_REPOSITORY}/alpaca:${ISLE_TAG}
    environment:
      # Increase the throughput of consumption from the queue.
      ALPACA_ACTIVEMQ_CONNECTIONS: 10
      ALPACA_ACTIVEMQ_CONSUMERS: 2
  crayfits:
    <<: *common
    image: ${ISLE_REPOSITORY}/crayfits:${ISLE_TAG}
  fits:
    <<: *common
    image: ${ISLE_REPOSITORY}/fits:${ISLE_TAG}
  homarus:
    <<: *common
    image: ${ISLE_REPOSITORY}/homarus:${ISLE_TAG}
  houdini:
    <<: *common
    image: ${ISLE_REPOSITORY}/houdini:${ISLE_TAG}
  hypercube:
    <<: *common
    image: ${ISLE_REPOSITORY}/hypercube:${ISLE_TAG}
  mariadb:
    <<: *common
    image: ${ISLE_REPOSITORY}/mariadb:${ISLE_TAG}
    volumes:
      - mariadb-data:/var/lib/mysql:rw
  milliner:
    <<: *common
    image: ${ISLE_REPOSITORY}/milliner:${ISLE_TAG}
  activemq:
    <<: *common
    image: ${ISLE_REPOSITORY}/activemq:${ISLE_TAG}
    labels:
      <<: *traefik-https-redirect-middleware
      traefik.http.routers.activemq_http.entrypoints: http
      traefik.http.routers.activemq_http.middlewares: *traefik-https-redirect
      traefik.http.routers.activemq_http.rule: &traefik-host-activemq Host(`activemq.islandora.dev`)
      traefik.http.routers.activemq_http.service: activemq
      traefik.http.routers.activemq_https.entrypoints: https
      traefik.http.routers.activemq_https.rule: *traefik-host-activemq
      traefik.http.routers.activemq_https.tls: true
      traefik.http.services.activemq.loadbalancer.server.port: 8161
      traefik.subdomain: activemq
    volumes:
      - activemq-data:/opt/activemq/data:rw
  cantaloupe:
    <<: *common
    image: ${ISLE_REPOSITORY}/cantaloupe:${ISLE_TAG}
    labels:
      <<: *traefik-https-redirect-middleware
      traefik.http.middlewares.cantaloupe-custom-request-headers.headers.customrequestheaders.X-Forwarded-Path: /cantaloupe
      traefik.http.middlewares.cantaloupe-strip-prefix.stripprefix.prefixes: /cantaloupe
      traefik.http.middlewares.cantaloupe.chain.middlewares: cantaloupe-strip-prefix,cantaloupe-custom-request-headers
      traefik.http.routers.cantaloupe_http.entrypoints: http
      traefik.http.routers.cantaloupe_http.middlewares: *traefik-https-redirect
      traefik.http.routers.cantaloupe_http.rule: &traefik-host-cantaloupe Host(`islandora.dev`) && PathPrefix(`/cantaloupe`)
      traefik.http.routers.cantaloupe_http.service: cantaloupe
      traefik.http.routers.cantaloupe_https.middlewares: cantaloupe
      traefik.http.routers.cantaloupe_https.entrypoints: https
      traefik.http.routers.cantaloupe_https.rule: *traefik-host-cantaloupe
      traefik.http.routers.cantaloupe_https.tls: true
      traefik.http.services.cantaloupe.loadbalancer.server.port: 8182
    volumes:
      - cantaloupe-data:/data:rw
  drupal:
    <<: *common
    image: ${REPOSITORY}/drupal:${TAG}
    environment: &drupal-environment
      # Keep this in sync with "islandora.drupal.properties" in the helm chart.
      DEVELOPMENT_ENVIRONMENT: "true"
      DRUPAL_DEFAULT_CANTALOUPE_URL: "https://islandora.dev/cantaloupe/iiif/2"
      DRUPAL_DEFAULT_INSTALL_EXISTING_CONFIG: "true"
      DRUPAL_DEFAULT_MATOMO_URL: "https://islandora.dev/matomo/"
      DRUPAL_DEFAULT_NAME: "CWRC/CSEC"
      DRUPAL_DEFAULT_PROFILE: "minimal"
      DRUPAL_DEFAULT_SITE_URL: "islandora.dev"
      DRUPAL_DEFAULT_SOLR_CORE: "default"
      DRUPAL_DRUSH_URI: "https://islandora.dev" # Used by docker/drupal/rootfs/usr/local/share/custom/install.sh
    volumes:
      # Allow code-server to serve Drupal / override it.
      - &drupal-root
        type: volume
        source: drupal-root
        target: /var/www/drupal
      - &drupal-public-files
        type: volume
        source: drupal-public-files
        target: /var/www/drupal/web/sites/default/files
      - &drupal-private-files
        type: volume
        source: drupal-private-files
        target: /var/www/drupal/private
      # Reflect changes from running composer update or modifying configuration
      # and custom modules in the container.
      - &drupal-assets ./docker/drupal/rootfs/var/www/drupal/assets:/var/www/drupal/assets:${CONSISTENCY}
      - &drupal-patches ./docker/drupal/rootfs/var/www/drupal/patches:/var/www/drupal/patches:${CONSISTENCY}
      - &drupal-config ./docker/drupal/rootfs/var/www/drupal/config:/var/www/drupal/config:${CONSISTENCY}
      - &drupal-content ./docker/drupal/rootfs/var/www/drupal/content:/var/www/drupal/content:${CONSISTENCY}
      - &drupal-custom-modules ./docker/drupal/rootfs/var/www/drupal/web/modules/custom:/var/www/drupal/web/modules/custom:${CONSISTENCY}
      - &drupal-custom-themes ./docker/drupal/rootfs/var/www/drupal/web/themes/custom:/var/www/drupal/web/themes/custom:${CONSISTENCY}
      - &drupal-composer-json ./docker/drupal/rootfs/var/www/drupal/composer.json:/var/www/drupal/composer.json:${CONSISTENCY}
      - &drupal-composer-lock ./docker/drupal/rootfs/var/www/drupal/composer.lock:/var/www/drupal/composer.lock:${CONSISTENCY}
    build:
      <<: *build-common
      context: ./docker/drupal
  matomo:
    <<: *common
    image: ${ISLE_REPOSITORY}/matomo:${ISLE_TAG}
    labels:
      <<: *traefik-https-redirect-middleware
      traefik.http.middlewares.matomo-customrequestheaders.headers.customrequestheaders.X-Forwarded-Uri: /matomo
      traefik.http.middlewares.matomo-stripprefix.stripprefix.prefixes: /matomo
      traefik.http.middlewares.matomo.chain.middlewares: matomo-stripprefix,matomo-customrequestheaders
      traefik.http.routers.matomo_http.entrypoints: http
      traefik.http.routers.matomo_http.middlewares: *traefik-https-redirect
      traefik.http.routers.matomo_http.rule: &traefik-host-matomo Host(`islandora.dev`) && PathPrefix(`/matomo`)
      traefik.http.routers.matomo_http.service: matomo
      traefik.http.routers.matomo_https.entrypoints: https
      traefik.http.routers.matomo_https.middlewares: matomo
      traefik.http.routers.matomo_https.rule: *traefik-host-matomo
      traefik.http.routers.matomo_https.tls: true
      traefik.http.services.matomo.loadbalancer.server.port: 80
    environment:
      MATOMO_DEFAULT_HOST: "https://islandora.dev"
    volumes:
      - matomo-config-data:/var/www/matomo:rw
  solr:
    <<: *common
    image: ${REPOSITORY}/solr:${TAG}
    labels:
      <<: *traefik-https-redirect-middleware
      traefik.http.routers.solr_http.entrypoints: http
      traefik.http.routers.solr_http.middlewares: *traefik-https-redirect
      traefik.http.routers.solr_http.rule: &traefik-host-solr Host(`solr.islandora.dev`)
      traefik.http.routers.solr_http.service: solr
      traefik.http.routers.solr_https.entrypoints: https
      traefik.http.routers.solr_https.rule: *traefik-host-solr
      traefik.http.routers.solr_https.tls: true
      traefik.http.services.solr.loadbalancer.server.port: 8983
    environment:
      SOLR_JAVA_OPTS: "-Dsolr.data.home=/data"
    volumes:
      - solr-data:/data:rw
    build:
      <<: *build-common
      context: ./docker/solr
  traefik:
    <<: *common
    image: traefik:v2.11.14
    command: >-
      --api.insecure=true
      --api.dashboard=true
      --api.debug=true
      --entryPoints.http.address=:80
      --entryPoints.https.address=:443
      --entryPoints.ssh.address=:22
      --providers.file.filename=/etc/traefik/tls.yml
      --providers.docker=true
      --providers.docker.network=default
      --providers.docker.exposedByDefault=false
      '--providers.docker.defaultRule=Host(`{{index .Labels "com.docker.compose.service" }}.islandora.dev`)'
    labels:
      <<: *traefik-https-redirect-middleware
      traefik.http.routers.traefik_http.entrypoints: http
      traefik.http.routers.traefik_http.middlewares: *traefik-https-redirect
      traefik.http.routers.traefik_http.service: traefik
      traefik.http.routers.traefik_https.entrypoints: https
      traefik.http.routers.traefik_https.tls: true
      traefik.http.services.traefik.loadbalancer.server.port: 8080
    ports:
      - "80:80"
      - "443:443"
      - "2222:22"
    volumes:
      - ./build/certs:/etc/ssl/traefik:rw
      - ./tls.yml:/etc/traefik/tls.yml:rw
      - /var/run/docker.sock:/var/run/docker.sock:ro
    networks:
      default:
        aliases:
          # Allow services to connect on the same name/port as the outside.
          - islandora.dev
          - solr.islandora.dev
    depends_on:
      # Sometimes traefik doesn't pick up on new containers so make sure they are started before traefik.
      - activemq
      - drupal
      - ide
      - solr
  ide:
    <<: *common
    image: ${REPOSITORY}/code-server:${TAG}
    labels:
      <<: *traefik-https-redirect-middleware
      # All Drupal traefik is routed through the IDE so that XDebug can be
      # easily used.
      traefik.http.routers.drupal_http.entrypoints: http
      traefik.http.routers.drupal_http.middlewares: *traefik-https-redirect
      traefik.http.routers.drupal_http.rule: &traefik-host-drupal Host(`islandora.dev`)
      traefik.http.routers.drupal_http.service: drupal
      traefik.http.routers.drupal_https.entrypoints: https
      traefik.http.routers.drupal_https.rule: *traefik-host-drupal
      traefik.http.routers.drupal_https.service: drupal
      traefik.http.routers.drupal_https.tls: true
      traefik.http.services.drupal.loadbalancer.server.port: 80
      traefik.http.routers.ide_http.entrypoints: http
      traefik.http.routers.ide_http.middlewares: *traefik-https-redirect
      traefik.http.routers.ide_http.rule: &traefik-host-ide Host(`ide.islandora.dev`)
      traefik.http.routers.ide_http.service: ide
      traefik.http.routers.ide_https.entrypoints: https
      traefik.http.routers.ide_https.rule: *traefik-host-ide
      traefik.http.routers.ide_https.service: ide
      traefik.http.routers.ide_https.tls: true
      traefik.http.services.ide.loadbalancer.server.port: 8443
      traefik.tcp.routers.ssh.entrypoints: ssh
      traefik.tcp.routers.ssh.rule: HostSNI(`*`)
      traefik.tcp.routers.ssh.service: ssh
      traefik.tcp.services.ssh.loadbalancer.server.port: 22

    environment:
      <<: *drupal-environment
      #enable the development environment
      DEVELOPMENT_ENVIRONMENT: "true"
      # Allow XDebug to be used with Drush as well.
      # Use the following command in the IDE shell to enable it:
      # export XDEBUG_SESSION=1
      DRUSH_ALLOW_XDEBUG: 1
      XDEBUG_MODE: debug
      # Do not request a password for accessing the IDE.
      CODE_SERVER_AUTHENTICATION: none
      # Bump up time outs to allow for debugging.
      NGINX_CLIENT_BODY_TIMEOUT: 600s
      NGINX_FASTCGI_CONNECT_TIMEOUT: 600s
      NGINX_FASTCGI_READ_TIMEOUT: 1200s
      NGINX_FASTCGI_SEND_TIMEOUT: 600s
      NGINX_KEEPALIVE_TIMEOUT: 750s
      NGINX_LINGERING_TIMEOUT: 50s
      NGINX_PROXY_CONNECT_TIMEOUT: 600s
      NGINX_PROXY_READ_TIMEOUT: 600s
      NGINX_PROXY_SEND_TIMEOUT: 600s
      NGINX_SEND_TIMEOUT: 600s
      PHP_DEFAULT_SOCKET_TIMEOUT: 600
      PHP_MAX_EXECUTION_TIME: 300
      PHP_MAX_INPUT_TIME: 600
      PHP_PROCESS_CONTROL_TIMEOUT: 600
      PHP_REQUEST_TERMINATE_TIMEOUT: 600
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      # Mount and serve contents of Drupal site.
      - <<: *drupal-root
        volume:
          nocopy: true
      # Mount and serve Drupal public files.
      - <<: *drupal-public-files
        volume:
          nocopy: true
      # Mount and serve Drupal private files.
      - <<: *drupal-private-files
        volume:
          nocopy: true
      # Volumes for code-server cache.
      - type: volume
        source: code-server-data
        target: /opt/code-server/data
      - type: volume
        source: jetbrains-cache
        target: /var/lib/nginx/.cache/JetBrains
      - type: volume
        source: jetbrains-config
        target: /var/lib/nginx/.config/JetBrains
      - *drupal-assets
      - *drupal-patches
      - *drupal-config
      - *drupal-content
      - *drupal-custom-modules
      - *drupal-custom-themes
      # Reflect changes from running composer update or modifying configuration
      # and custom modules in the IDE container. Files are a special case as we
      # can't bind mount them to both containers and have them work consistently.
      - *drupal-composer-json
      - *drupal-composer-lock
    build:
      <<: *build-common
      context: ./docker/ide
    # Ensure drupal mounts the shared volumes first.
    depends_on:
      - drupal
